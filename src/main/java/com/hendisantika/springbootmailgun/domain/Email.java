package com.hendisantika.springbootmailgun.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mailgun
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/21
 * Time: 14.19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Email {
    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String subject;

    @NotNull
    private String url;

    @NotNull
    @Size(min = 10)
    private String messageText;

}
