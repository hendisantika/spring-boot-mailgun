# spring-boot-mailgun-sample

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-mailgun.git`
2. Navigate to the folder: `cd spring-boot-mailgun`
3. Create [MailGun](https://mailgun.com/) Account and set SMTP & API KEY
4. Change SMTP & API KEY with your MailGun credentials
5. Run the application: `mvn clean spring-boot:run`
6. Open your favorite browser: http://localhost:8080/sendEmail - Send basic email using SMTP
7. Open your favorite browser: http://localhost:8080/sendEmail2 - Send basic email using API KEY
8. Open your favorite browser: http://localhost:8080/sendEmail3 - Send basic email using MailGun Template
9. Open your favorite browser: http://localhost:8080/sendEmail4 - Send basic email using another MailGun Lib
10. Open your favorite browser: http://localhost:8080/sendEmail5 - Send basic email using another MailGun Lib