package com.hendisantika.springbootmailgun.domain;

import lombok.Data;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mailgun
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/21
 * Time: 14.31
 */
@Data
public class Email2 {
    private String from;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String subject;
    private String message;

    public Email2() {
    }

    public Email2(String from, List<String> to, List<String> cc, List<String> bcc) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
    }

    public Email2(String from, List<String> to, List<String> cc, List<String> bcc, String subject, String message) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.message = message;
    }
}
