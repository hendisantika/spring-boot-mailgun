package com.hendisantika.springbootmailgun.service;

import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mailgun
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/21
 * Time: 14.34
 */
@Log4j2
@Service
public class EmailService {

    static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Value("${email.mailgun.apiKey}")
    private String mailgunApiKey;

    @Value("${email.mailgun.host}")
    private String mailgunHost;

}
