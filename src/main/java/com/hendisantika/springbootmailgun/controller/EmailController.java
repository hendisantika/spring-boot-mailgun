package com.hendisantika.springbootmailgun.controller;

import com.hendisantika.springbootmailgun.config.EmailConfig;
import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.model.message.Message;
import com.mailgun.model.message.MessageResponse;
import com.mailgun.util.EmailUtil;
import lombok.extern.slf4j.Slf4j;
import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mailgun
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/21
 * Time: 14.25
 */
@RestController
@Slf4j
public class EmailController {

    private final EmailConfig emailConfig;

    @Autowired
    private MailgunMessagesApi mailgunMessagesApi;

    // this constructor performs autowired DIa
    public EmailController(EmailConfig emailConfig) {
        this.emailConfig = emailConfig;
    }

    @GetMapping("/sendEmail")
    public ResponseEntity<String> sendEmail() {
        // create mail sender

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(this.emailConfig.getHost());
        mailSender.setPort(this.emailConfig.getPort());
        mailSender.setUsername(this.emailConfig.getUsername());
        mailSender.setPassword(this.emailConfig.getPassword());

        // create an email instance

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        // email.getEmail()
        mailMessage.setFrom("Aktivasi anaonline <no-reply@anaonline.id>");
        mailMessage.setTo("hendi@yopmail.com");
        // email.getSubject()
        mailMessage.setSubject("SMTP - E-Voting Token Instruction Test subject");
        // email.getMessageText()
        mailMessage.setText("Test text !!!\nSend Email using SMTP");

        // send mail
        mailSender.send(mailMessage);
        log.info("Email already sent via SMTP! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }

    @GetMapping("/sendEmail2")
    public ResponseEntity<String> sendEmail2() {
        // create mail sender
        Configuration configuration = new Configuration()
                .domain(this.emailConfig.getDomain())
                .apiKey(this.emailConfig.getApiKey())
                .from("Aktivasi anaonline", "no-reply@anaonline.id");

        Mail.using(configuration)
                .to("hendi@yopmail.com")
                .subject("API KEY - This is the subject")
                .text("Hello world!\nSend Email using API KEY")
                .build()
                .send();
        log.info("Email already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }

    @GetMapping("/sendEmail3")
    public ResponseEntity<String> sendEmail3() {
        // create mail sender
        Configuration configuration = new Configuration()
                .domain(this.emailConfig.getDomain())
                .apiKey(this.emailConfig.getApiKey())
                .from("Aktivasi anaonline", "no-reply@anaonline.id");

        Mail.using(configuration)
                .to("hendi@yopmail.com")
                .subject("API KEY - This is the subject")
//                .template("account_activation")
                .template("testing")
                .parameter("v:fullName", "Uzumaki Naruto")
                .build()
                .send();

        log.info("Email with Template already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }

    @GetMapping("/sendEmail4")
    public ResponseEntity<String> sendEmail4() {
        Message message = Message.builder()
                .from("Aktivasi Anaonline <aktivasi@anaonline.id>")
                .to(EmailUtil.nameWithEmail("USER_NAME", "naruto@yopmail.com"))
                .subject("SUBJECT")
                .text("TEXT2\nEmail with Template already sent via API KEY! Please check your inbox for order confirmation!")
                .build();

        MessageResponse messageResponse = mailgunMessagesApi.sendMessage(this.emailConfig.getDomain(), message);

        log.info("messageResponse: " + messageResponse);
        log.info("Email with Template already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }

    @GetMapping("/sendEmail5")
    public ResponseEntity<String> sendEmail5() {
        Message message = Message.builder()
                .from("Aktivasi Anaonline <aktivasi@anaonline.id>")
                .to(EmailUtil.nameWithEmail("USER_NAME", "naruto@yopmail.com"))
                .subject("SUBJECT")
                .text("TEXT2\nEmail with Template already sent via API KEY! Please check your inbox for order confirmation!")
                .build();

        CompletableFuture<MessageResponse> result = mailgunMessagesApi.sendMessageAsync(this.emailConfig.getDomain(), message);
        log.info("Email with Template already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }
}
