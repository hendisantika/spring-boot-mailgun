package com.hendisantika.springbootmailgun;

import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.api.v3.MailgunTemplatesApi;
import com.mailgun.client.MailgunClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootMailgunApplication {

    private static final String PRIVATE_API_KEY = "YOUR_PRIVATE_API_KEY";

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMailgunApplication.class, args);
    }

    @Bean
    public MailgunMessagesApi mailgunMessagesApi() {
        return MailgunClient.config(PRIVATE_API_KEY)
                .createApi(MailgunMessagesApi.class);
    }

    @Bean
    public MailgunMessagesApi mailgunMessagesApiSync() {
        return MailgunClient.config(PRIVATE_API_KEY)
                .createAsyncApi(MailgunMessagesApi.class);
    }

    @Bean
    public MailgunTemplatesApi mailgunTemplatesApi() {
        return MailgunClient.config(PRIVATE_API_KEY)
                .createApi(MailgunTemplatesApi.class);
    }
}
